import React from 'react';
export default class NewsItem extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="item">
        <div className="item-image">
          <img className="item-image-content" src={this.props.image} />
        </div>

        <div className="item-content">
          <div className="title">{this.props.title}</div>

          <div className="item-content-author">
            <div className="author">{this.props.author}</div>
            <div>{this.props.date}</div>
          </div>
          <div>{this.props.description}</div>
        </div>
      </div>
    );
  }
}
