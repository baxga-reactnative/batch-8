import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders news', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/news/i);
  expect(linkElement).toBeInTheDocument();
});
