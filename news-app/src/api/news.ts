import axios from 'axios';

export function fetchListNews() {
  return {
    data: {
      articles: [
        {
          id: 1,
          title: 'ABC',
          author: 'Author Name',
          publishedAt: 'date',
          urlToImage: '',
        },
      ],
    },
  };
}
