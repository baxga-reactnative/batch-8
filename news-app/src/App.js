import React from 'react';
// import logo from './logo.svg';
import './App.css';
import { fetchListNews } from './api/news';
import NewsItem from './NewsItem.component';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listNews: [
        {
          id: 1,
          title: 'ABC',
          author: 'Author Name',
          publishedAt: 'date',
          urlToImage: '',
        },
      ],
    };
  }

  componentDidMount() {
    // fetchListNews()
    //   .then((res) => {
    //     console.info(res);
    //     this.setState({ listNews: res.data.articles });
    //   })
    //   .catch((err) => {
    //     console.error(err);
    //   });
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">NEWS FOR YOU</header>

        <main>
          {this.state.listNews.map((item, index) => (
            <NewsItem
              key={'news_' + index}
              image={item.urlToImage}
              title={item.title}
            />
          ))}

          {this.state.listNews.map((item, index) => (
            <NewsItem
              key={'person_' + index}
              image={item.urlToImage}
              title={item.title}
            />
          ))}
        </main>
      </div>
    );
  }
}
