import { Injectable } from '@nestjs/common';
import { listContent } from '../mockData/content';

@Injectable()
export class ContentService {
  constructor() {}

  async search(interest: string) {
    try {
      const array = listContent.filter((item) =>
        item.interests.includes(interest),
      );

      return array;
    } catch (err) {
      return Promise.reject(err);
    }
  }
}
