import { Controller, HttpException, Param, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ContentService } from './content.service';

@ApiTags('Content')
@Controller('content')
export class ContentController {
  constructor(public service: ContentService) {}

  @Get(':interest')
  getOne(@Param('interest') interest: string) {
    try {
      return this.service.search(interest);
    } catch (err) {
      throw new HttpException(err.message, err.status);
    }
  }
}
