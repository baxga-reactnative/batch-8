import { Injectable } from '@nestjs/common';
import { AuthLoginDto } from './auth.dto';
import { listUser } from '../mockData/user';

@Injectable()
export class AuthService {
  constructor() {}

  async login(dto: AuthLoginDto) {
    try {
      const findUser = listUser.find(
        (item) =>
          item.username === dto.username && item.password === dto.password,
      );

      if (findUser) {
        delete findUser.password;

        return {
          access_token: findUser.access_token,
          refresh_token: findUser.access_token,
        };
      } else {
        return Promise.reject({ status: 401, message: 'Unauthorized' });
      }
    } catch (err) {
      return Promise.reject(err);
    }
  }
}
