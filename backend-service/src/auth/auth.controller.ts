import { Body, Controller, HttpException, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { AuthLoginDto } from './auth.dto';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(public service: AuthService) {}

  @Post('login')
  login(@Body() dto: AuthLoginDto) {
    try {
      return this.service.login(dto);
    } catch (err) {
      throw new HttpException(err.message, err.status);
    }
  }
}
