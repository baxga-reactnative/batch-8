import { listUser } from './user';

export const userInterests = [
  {
    user_id: listUser[0].id,
    interests: ['REACT_NATIVE', 'REACT_JS', 'NODE_JS', 'REDUX'],
  },
  {
    user_id: listUser[1].id,
    interests: ['HTML', 'REACT_JS', 'BUSINESS'],
  },
  {
    user_id: listUser[2].id,
    interests: ['TYPESCRIPT', 'REACT_NATIVE', 'REDUX', 'REDUX_SAGA'],
  },
];
