export const listUser = [
  {
    id: 1,
    username: 'demo',
    password: 'demo123',
    first_name: 'Demo',
    last_name: 'User',
    photo_url:
      'https://image.freepik.com/free-vector/cartoon-monster-face-avatar-halloween-monster_6996-1164.jpg',
    access_token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJkZW1vIiwiZmlyc3RfbmFtZSI6IkRlbW8iLCJsYXN0X25hbWUiOiJVc2VyIiwicGhvdG9fdXJsIjoiaHR0cHM6Ly9pbWFnZS5mcmVlcGlrLmNvbS9mcmVlLXZlY3Rvci9jYXJ0b29uLW1vbnN0ZXItZmFjZS1hdmF0YXItaGFsbG93ZWVuLW1vbnN0ZXJfNjk5Ni0xMTY0LmpwZyJ9.5dRHNv_FVypXZwruZfkQhm0gziG_MvUJhuPugF8Trnw',
  },
  {
    id: 2,
    username: 'andre',
    password: 'andre123',
    first_name: 'Andre',
    last_name: 'Tauaja',
    photo_url:
      'https://image.freepik.com/free-vector/cartoon-monster-face-avatar-halloween-monster_6996-1154.jpg',
    access_token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlcm5hbWUiOiJhbmRyZSIsImZpcnN0X25hbWUiOiJBbmRyZSIsImxhc3RfbmFtZSI6IlRhdWFqYSIsInBob3RvX3VybCI6Imh0dHBzOi8vaW1hZ2UuZnJlZXBpay5jb20vZnJlZS12ZWN0b3IvY2FydG9vbi1tb25zdGVyLWZhY2UtYXZhdGFyLWhhbGxvd2Vlbi1tb25zdGVyXzY5OTYtMTE1NC5qcGcifQ.wChtOlIv3W9yFd7XYWVduIUxuKzCZv-vO7pvKLsP9xE',
  },
  {
    id: 3,
    username: 'budi',
    password: 'budi123',
    first_name: 'Budi',
    last_name: 'Fasolasido',
    photo_url:
      'https://image.freepik.com/free-vector/cartoon-monster-face-avatar-halloween-monster_6996-1174.jpg',
    access_token:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywidXNlcm5hbWUiOiJidWRpIiwiZmlyc3RfbmFtZSI6IkJ1ZGkiLCJsYXN0X25hbWUiOiJGYXNvbGFzaWRvIiwicGhvdG9fdXJsIjoiaHR0cHM6Ly9pbWFnZS5mcmVlcGlrLmNvbS9mcmVlLXZlY3Rvci9jYXJ0b29uLW1vbnN0ZXItZmFjZS1hdmF0YXItaGFsbG93ZWVuLW1vbnN0ZXJfNjk5Ni0xMTc0LmpwZyJ9.PHcFNM9OaX_sFn_A2UTwqXVBvO7mOdTcS-FhyFjgArg',
  },
];
