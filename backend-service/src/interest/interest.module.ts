import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InterestController } from './interest.controller';
import { InterestService } from './interest.service';

@Module({
  imports: [TypeOrmModule.forFeature([])],

  providers: [InterestService],

  controllers: [InterestController],
})
export class InterestModule {}
