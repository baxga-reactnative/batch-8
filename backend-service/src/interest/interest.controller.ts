import { Controller, HttpException, Param, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { InterestService } from './interest.service';

@ApiTags('Interest')
@Controller('interest')
export class InterestController {
  constructor(public service: InterestService) {}

  @Get(':user_id')
  getOne(@Param('user_id') user_id: string) {
    try {
      return this.service.getOne(user_id);
    } catch (err) {
      throw new HttpException(err.message, err.status);
    }
  }
}
