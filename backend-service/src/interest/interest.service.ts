import { Injectable } from '@nestjs/common';
import { userInterests } from '../mockData/interest';

@Injectable()
export class InterestService {
  constructor() {}

  async getOne(user_id: string) {
    try {
      const findOne = userInterests.find(
        (item) => String(item.user_id) === String(user_id),
      );

      if (findOne) {
        return findOne;
      } else {
        return Promise.reject({ status: 404, message: 'Not Found' });
      }
    } catch (err) {
      return Promise.reject(err);
    }
  }
}
