import { Injectable } from '@nestjs/common';
import { listUser } from '../mockData/user';

@Injectable()
export class ProfileService {
  constructor() {}

  async getOne(id: string) {
    try {
      const findUser = listUser.find((item) => String(item.id) === String(id));

      if (findUser) {
        delete findUser.password;
        delete findUser.access_token;

        return findUser;
      } else {
        return Promise.reject({ status: 404, message: 'Not Found' });
      }
    } catch (err) {
      return Promise.reject(err);
    }
  }
}
