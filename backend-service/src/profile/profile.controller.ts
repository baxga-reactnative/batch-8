import { Controller, HttpException, Param, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ProfileService } from './profile.service';

@ApiTags('Profile')
@Controller('profile')
export class ProfileController {
  constructor(public service: ProfileService) {}

  @Get(':id')
  getOne(@Param('id') id: string) {
    try {
      return this.service.getOne(id);
    } catch (err) {
      throw new HttpException(err.message, err.status);
    }
  }
}
