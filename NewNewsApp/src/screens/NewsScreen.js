import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import NewsItem from '../components/NewsItem.component';

export default class NewsScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      listNews: [
        {
          id: 1,
          title: 'BERITA 1',
          author: 'Author Name',
          publishedAt: '1 Okt 2020',
          urlToImage:
            'https://image.freepik.com/free-photo/image-human-brain_99433-298.jpg',
          description: 'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
        },
        {
          id: 2,
          title: 'BERITA 2',
          author: 'Author Name',
          publishedAt: '2 Okt 2020',
          urlToImage:
            'https://image.freepik.com/free-photo/image-human-brain_99433-298.jpg',
          description: 'Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum',
        },
      ],
    };
  }

  render() {
    return (
      <View>
        <View>
          <Text style={styles.header}>NEWS FOR YOU</Text>
        </View>

        <View>
          {this.state.listNews.map((news, index) => (
            <NewsItem
              key={index}
              image={news.urlToImage}
              title={news.title}
              author={news.author}
              date={news.publishedAt}
              description={news.description}></NewsItem>
          ))}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
