import React from 'react';
import {View, Text, Button} from 'react-native';
import {connect} from 'react-redux';
import CarouselComponent from '../components/Carousel.component';
import Jokes from '../components/Jokes.component';
import Weather from '../components/Weather.component';

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <View style={{marginTop: 20, padding: 20}}>
        <Text style={{fontWeight: 'bold', textAlign: 'center'}}>DASHBOARD</Text>

        <Text style={{fontWeight: 'bold'}}>Hi, {this.props.auth.username}</Text>

        <CarouselComponent />
        <Jokes />
        <Weather />

        <View style={{marginTop: 50}}>
          <Button
            onPress={() => this.props.navigation.navigate('Other')}
            title="GO TO EXAMPLE PAGE"
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  auth: state.auth,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
