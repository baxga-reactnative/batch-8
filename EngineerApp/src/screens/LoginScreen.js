import React, {useState} from 'react';
import {Text, View, Button, TextInput} from 'react-native';
import {connect} from 'react-redux';
import {loginAction} from '../redux/action/auth';

function LoginScreen(props) {
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);
  const [message, setMessage] = useState(null);

  const login = () => {
    if (!username) {
      setMessage('Username wajib diisi!');
    } else if (!password) {
      setMessage('Password wajib diisi!');
    } else {
      props.processLogin(username);
    }
  };

  return (
    <View>
      <Text>AYO LOGIN</Text>

      <Text style={{color: 'red'}}>{message}</Text>

      <TextInput
        onChangeText={(text) => setUsername(text)}
        value={username}
        style={{backgroundColor: '#ffffff'}}
        placeholder="Username"
      />

      <TextInput
        onChangeText={(text) => setPassword(text)}
        value={password}
        style={{backgroundColor: '#ffffff'}}
        secureTextEntry={true}
        placeholder="Password"
      />

      <Button color="#444" onPress={() => login()} title="LOGIN"></Button>
    </View>
  );
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
  processLogin: (textUsername) => dispatch(loginAction(textUsername)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
