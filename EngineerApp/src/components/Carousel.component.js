import React from 'react';
import {View, Image, Dimensions} from 'react-native';
import axios from 'axios';
import Carousel from 'react-native-snap-carousel';

export default class CarouselComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
    };
    this._carousel = null;
  }

  componentDidMount() {
    axios({
      method: 'GET',
      url: 'http://localhost:5000/promotions',
    })
      .then((res) => {
        console.info('carousel', res.data.length);
        this.setState({
          list: res.data,
        });
      })
      .catch((err) => {
        console.info('err', err);
      });
  }

  _renderItem = ({item, index}) => {
    return (
      <View>
        <Image
          source={{uri: item.preview_image}}
          style={{width: '100%', height: 200}}
        />
      </View>
    );
  };

  render() {
    // return <Text>AAA</Text>;
    return (
      <Carousel
        ref={(c) => {
          this._carousel = c;
        }}
        data={this.state.list}
        renderItem={this._renderItem}
        sliderWidth={Dimensions.get('window').width}
        itemWidth={Dimensions.get('window').width * 0.8}
      />
    );
  }
}
