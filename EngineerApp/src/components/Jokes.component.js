import React from 'react';
import {View, Text} from 'react-native';
import axios from 'axios';

export default class Jokes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      selectedData: {},
    };
  }

  componentDidMount() {
    axios({
      method: 'GET',
      url: 'http://localhost:5000/jokes',
    })
      .then((res) => {
        console.info('JOKES');
        this.setState({
          list: res.data,
        });

        this.intervalId = setInterval(() => {
          const randomNumber = Math.floor(
            Math.random() * this.state.list.length,
          );

          this.setState({selectedData: this.state.list[randomNumber]});
        }, 5000);
      })
      .catch((err) => {
        console.info('err', err);
      });
  }

  klik() {
    alert('HALOO');
  }

  componentWillUnmount() {
    clearInterval(this.intervalId);
  }

  render() {
    return (
      <View style={{marginTop: 20}}>
        <Text style={styles.title}>JOKES</Text>
        <Text>{JSON.stringify(this.state.selectedData)}</Text>

        {/* <TouchableOpacity style={{padding: 50}} onPress={() => this.klik()}>
          <Text>KLIK</Text>
        </TouchableOpacity> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {fontWeight: 'bold'},
});
