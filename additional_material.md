> More Short materials: https://drive.google.com/drive/folders/1V51uUrN5sFmMHlYOxCoQ789mOYX0x9a0?usp=sharing

> More complete materials: https://gist.github.com/isumizumi/d367c863f139a62d104211620b119f25

### FUNDAMENTAL
Short Materials:
- FUNdamental Challenge: https://docs.google.com/presentation/d/13edWzpCIboAuEUHDC77bvVWigSsl2v3Ng7LFT-VKKb8/edit?usp=sharing
- Promise & Async Await: https://docs.google.com/presentation/d/1GvOZT79mjG5KIIzvEKhg_1BaxxQc0kGXDDjVZ2_yuow/edit?usp=sharing
- Let's Fix Your Obstacle - OOP: https://docs.google.com/presentation/d/1Wdo-fvy_RG5wXZPslrUsF9cvHsPjMOgdnHnMDpgBZuk/edit?usp=sharing

### REACT NATIVE

- React Native Introduction: https://docs.google.com/presentation/d/1WGuBexJbvGzkaHGSFRUI_NQbTcao1jqziv6WjurT6y0/edit?usp=sharing
- React Native Introduction (Lifecycle): https://docs.google.com/presentation/d/1KxxURBrSHJjnE0rLUFQEt29T6ONsTZPHbyiWYOQokZ4/edit?usp=sharing
- Middleware - Redux Thunk & Saga: https://docs.google.com/presentation/d/19gesMT4ohg9P5X8QstznTzch6IIqFLcEiKBTXLDK91s/edit?usp=sharing
- Let's Implement Middleware - Redux Saga: https://docs.google.com/presentation/d/1l8dNwC_ESredT4DGNhoeWlKTzXV3PfNc8lMn_ikDMq8/edit?usp=sharing

### React Native Diagram
https://drive.google.com/file/d/1b6mjcRJD9gR9RnVfIZDQ7amAFbAVE-I_/view?usp=sharing

### ENGINEER APP WIREFRAME

- https://www.figma.com/file/YK0Tyrv5rmiT8F4zgRNMWV/Engineer-Social-App?node-id=2%3A250
